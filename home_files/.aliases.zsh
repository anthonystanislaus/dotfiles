# terraform
alias tf='terraform'
alias tfa='terraform apply'
alias tfaa='terraform apply --auto-approve'
alias tfar='terraform apply -refresh-only'
alias tfd='terraform destroy'
alias tfda='terraform destroy --auto-approve'
alias tff='terraform fmt'
alias tfg='terraform get'
alias tfiac='terraform -install-autocomplete'
alias tfi='terraform init'
alias tfim='terraform import'
alias tfli='terraform login'
alias tflo='terraform logout'
alias tfm='terraform metadata'
alias tfo='terraform output'
alias tfoj='terraform output -json'
alias tfp='terraform plan'
alias tfpr='terraform plan -refresh-only'
alias tfpro='terraform providers'
alias tfr='terraform refresh'
alias tfs='terraform show'
alias tfv='terraform validate'
alias tfviz='terraform graph'

tfcd() {
    local directory="$1"
    local subcommand="$2"

    if [ -z "$directory" ] || [ -z "$subcommand" ]; then
        echo "Usage: tfcd -chdir=<directory> <subcommand>"
        return 1
    fi

    case "$subcommand" in
        i)
            terraform -chdir="$directory" init
            ;;
        p|pr)
            if [[ "$subcommand" == "pr" ]]; then
                terraform -chdir="$directory" plan -refresh-only
            else
                terraform -chdir="$directory" plan
            fi
            ;;
        a|aa)
            if [[ "$subcommand" == "aa" ]]; then
                terraform -chdir="$directory" apply --auto-approve
            else
                terraform -chdir="$directory" apply
            fi
            ;;
        d|da)
            if [[ "$subcommand" == "da" ]]; then
                terraform -chdir="$directory" destroy --auto-approve
            else
                terraform -chdir="$directory" destroy
            fi
            ;;
        r)
            terraform -chdir="$directory" refresh
            ;;
        *)
            echo "Unknown subcommand: $subcommand"
            return 1
            ;;
    }
}

# gcloud
alias gc='gcloud'
alias gci='gcloud init'
alias gcprojd='gcloud project describe'
alias gccompi='gcloud components install'
alias gccompu='gcloud components update'
alias gcauthl='gcloud auth list'
alias gcauthli='gcloud auth login'
alias gcauthsa='gcloud auth activate-service-account'
