dotfiles
---

Base environment (https://search.nixos.org/packages)
- oh-my-zsh
- powerlevel10k (ohmyzsh plugin)
- gcloud
- aliases

Issues:
- TBD
