#!/usr/bin/env bash

echo '#################### START OF setup SCRIPT ####################'

REPO_ROOT="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
HOME_FILES="$REPO_ROOT/home_files"
BASE="$REPO_ROOT/base"

# --------------------------------------------------------------------

echo 'setup: loading nix env...'

source "$HOME/.nix-profile/etc/profile.d/nix.sh"

# --------------------------------------------------------------------

echo 'setup: installing nix packages...'

(
    nix-env -iA nixpkgs.fd \
        nixpkgs.bat \
        nixpkgs.fzf \
        nixpkgs.terraform \
        nixpkgs.google-cloud-sdk \
        nixpkgs.bun > /tmp/setup-nix.log 2>&1
) & disown

# --------------------------------------------------------------------

echo 'setup: installing base packages...'

TMPDIR=$(mktemp -d)

cd $TMPDIR

for file in ~/.dotfiles/base/*; do
  
  bash "$file" 2>&1

done > /tmp/setup-base.log

cd $REPO_ROOT && rm -rf $TMPDIR

# --------------------------------------------------------------------

# https://www.gitpod.io/docs/configure/user-settings/dotfiles#how-to-install-symlinks-from-dotfiles-when-using-a-custom-installation-script
echo 'setup: installing symlinks from dotfiles...'

while read -r file; do

    relative_file_path="${file#"${HOME_FILES}"/}"
    target_file="${HOME}/${relative_file_path}"
    target_dir="${target_file%/*}"

    if test ! -d "${target_dir}"; then
        mkdir -p "${target_dir}"
    fi

    printf '-- Installing symlink %s\n' "${target_file}"
    ln -sf "${file}" "${target_file}"

done < <(find "${HOME_FILES}" -type f)

# --------------------------------------------------------------------

# Load bash environment in zsh
# Taken from https://github.com/axonasif/bashenv.zsh
echo "set +m; source <(bash -lic 'declare -px'); set -m" >> "$HOME/.zshrc"

# --------------------------------------------------------------------

echo '#################### END OF setup SCRIPT ####################'
